package com.assessment.cartService.service.impl;

import com.assessment.cartService.entity.Cart;
import org.springframework.stereotype.Service;

@Service
public class CartServiceImpl extends CrudServiceImpl<Cart>
{
	// FIXME: add ProductServiceImpl dependency

	@Override
	protected Cart update(Cart input) {
		Cart dbCart = get(input.getId());
		if(dbCart == null) {
			throw new IllegalArgumentException("Attempting to update entity that does not exist");
		}
		// this update method will replace the stored products list with the list from the input
		// any logic to merge or remove items from the cart collection should happen in business logic prior to this
		dbCart.setProducts(input.getProducts());

		return dbCart;
	}

	// FIXME: applicant should finish the implementation of this method
	public Cart submitCart(Cart cart) throws Exception {

		// FIXME:  add required logic here for cart submission



		cart = save(cart);
		return cart;
	}
}

